resource "libvirt_network" "dev" {
  name      = var.network_name
  mode      = "nat"
  domain    = var.domain_name
  addresses = var.network_subnet
  autostart = true

  # I am running my own DHCP server.
  dhcp {
    enabled = false 
  }

  # NOTE: If 'dns -> enabled = true', each VM's resolv.conf will 
  #       automatically be set to the IP of the libvirt virtual network 
  #       switch (I am running my own DNS server).
  dns {
    enabled    = false 
    local_only = false 
  }
}

