variable "network_name" {
  type    = string
  default = ""
}

variable "domain_name" {
  type    = string
  default = ""
}

variable "network_subnet" {
  type    = list
  default = []
}

