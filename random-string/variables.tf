# "count" cannot be used as a variable name. It is reserved due to its 
# special meaning inside module blocks. Using 'instances' instead.
variable "instances" {
  type = number 
}

variable "length" {
  type = number 
}

# Control whether or not to use UPPERCASE
variable "upper" {
  type = bool
}

# Control whether or not to use lowercase
variable "lower" {
  type = bool
}

# Control whether or not to use integers
variable "number" {
  type = bool
}

# Control whether or not to use special characters
variable "special" {
  type = bool
}

