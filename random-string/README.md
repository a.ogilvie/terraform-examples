Generate a random string via Terraform's random provider
--------------------------------------------------------

## Tested on:

* CentOS CentOS-7.9.2009 (Core)
* CentOS-7-x86_64-Minimal-2009.iso
* Terraform 0.14.7
* Terraform hashicorp/random provider, version 3.1.0

## Dependencies:

* KVM
* Terraform 
* Terraform provider hashicorp/random 

## Instructions:

Edit `main.tf` and `variables.tf` as needed, then run as follows:

```sh
$ terraform init
$ terraform plan
$ terraform apply
```

Notes:

* The output `id=...` will be the random string that is generated.

* I am also using an `output.tf` file, which generates return values 
  that can be re-used to export information to other modules. 

* This module can be re-used in other Terraform configurations via a 
  `module` block:

  ```sh
  module "random_string" {
    source    = "../random-string"
    instances = var.instances
    length    = 6
    upper     = false
    lower     = true
    number    = true
    special   = false
  }
  ```

* Without using a `module` block elsewhere, settings can be tested by 
  using a `terraform.tfvars` file similar to the example below: 

  ```sh
  $ vi terraform.tfvars
    instances = 1
    length    = 6
    upper     = false 
    lower     = true
    number    = true
    special   = false
  ```

