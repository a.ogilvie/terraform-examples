resource "random_string" "string_name" {
  count  = var.instances

  # 'length' is mandatory.
  length  = var.length

  upper   = var.upper 
  lower   = var.lower 
  number  = var.number 
  special = var.special
}

