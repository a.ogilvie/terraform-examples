output "string_name" {
  value = random_string.string_name.*.id
}

output "string_count" {
  value = length(random_string.string_name.*.id)
}
