resource "libvirt_pool" "dev" {
  name = var.pool_name
  # Currently, only 'dir' is supported.
  type = "dir"
  path = "/${var.pool_path}/${var.pool_name}"
}

