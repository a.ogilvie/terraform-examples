Instructions for provisioning and managing storage pools in Linux KVM
---------------------------------------------------------------------

## Tested on:

* CentOS CentOS-7.9.2009 (Core)
* CentOS-7-x86_64-Minimal-2009.iso
* Terraform 0.14.7
* Terraform Provider registry.terraform.io/dmacvicar/libvirt v0.6.3

## Dependencies:

* KVM
* Terraform 
* Terraform provider libvirt 

## Instructions:

Edit `main.tf`, and `variables.tf` as needed. Afterwards, create file 
`terraform.tfvars`. For example:

```sh
$ vi terraform.tfvars
pool_name = "dev"
# Will automatically be called as '/data'.
pool_path = "data"
```

Finally, run as follows:

```sh
$ terraform init
$ terraform plan
$ terraform apply
```

